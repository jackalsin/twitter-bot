# 垂死病中惊坐起

<!-- TOC -->

- [垂死病中惊坐起](#垂死病中惊坐起)
    - [1. 简介](#1-简介)
    - [2. 使用说明](#2-使用说明)
        - [2.1 申请一个twitter开发者账户](#21-申请一个twitter开发者账户)
        - [2.2 下载代码](#22-下载代码)
        - [2.3 云端部署](#23-云端部署)
    - [3. 参数设置](#3-参数设置)
    - [4. Debug](#4-debug)
    - [5. 更新](#5-更新)
    - [6. Reference](#6-reference)
    - [7. License](#7-license)

<!-- /TOC -->

## 1. 简介

这个repo是为了让广大人民群众追星用的，如果你喜欢某个明星，辣么你就可以用这个来尾随他的tweet，并且喜欢和转发他的推特

## 2. 使用说明

### 2.1 申请一个twitter开发者账户

来到[这里](https://developer.twitter.com/en/apps)，登录您的推特账号

点击**Create an app**
![create an app](./images/twitter-create-an-app.png)

选择 making a bot
![choose a bot](./images/twitter-create-an-app-choose-bot.png)

说说啥理由
![reason](./images/twitter-create-an-app-reason.png)

这里你可以谈你只是转发和喜爱推特

同一个页面上，我们选择正确选项，
![reason](./images/twitter-create-an-app-reason-2.png)

一路下去提交申请

- 此处会有些账户被迫人工审核，只能等

来到 [app 页面](https://developer.twitter.com/en/apps)
选择刚创建好的
![key and token](./images/twitter-create-an-app-key-and-tokens.png)

Get key and secret

![Get key and token](./images/get-key-and-token.png)

将以上四个设置为环境变量

1. `TWITTER_CONSUMER_KEY`
1. `TWITTER_CONSUMER_SECRET`
1. `TWITTER_ACCESS_TOKEN`
1. `TWITTER_ACCESS_TOKEN_SECRET`

Windows 10 可以按 Win + S, 输入 env，弹出环境变量

![env-set](./images/env-set.png)
依次添加

Mac 我买不起，你们可以参照以下linux的

```bash
export TWITTER_CONSUMER_KEY=xxxxxxxxxxxxx
export TWITTER_CONSUMER_SECRET=xxxxxxxxxxxxx
export TWITTER_ACCESS_TOKEN=xxxxxxxxxxxxx
export TWITTER_ACCESS_TOKEN_SECRET=xxxxxxxxxxxxx
```

### 2.2 下载代码

安装

- [git](https://git-scm.com/downloads)
- [node](https://nodejs.org/en/), 选择LTS

直接下载源代码到本地，进入文件夹，用cmd或者bash跑

```bash
npm install
npm start
```

那么你就能看见你会转发你偶像川普的一些推特

### 2.3 云端部署

来到`heroku`， 注册一个账号，完全免费

首先，**create an app**

![heroku-create-app](./images/heroku-create-app.png)

然后来到`Settings`
![settings](./images/heroku-set-env.png)

把四个环境变量再加一边

```bash
TWITTER_CONSUMER_KEY=xxxxxxxxxxxxx
TWITTER_CONSUMER_SECRET=xxxxxxxxxxxxx
TWITTER_ACCESS_TOKEN=xxxxxxxxxxxxx
TWITTER_ACCESS_TOKEN_SECRET=xxxxxxxxxxxxx
```

download and install [Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli)

来到刚才源码下载的地方，开个cmd/bash/terminal

```bash
heroku login

git init

git add .
git commit -m "I love idol"

heroku git:remote -a <此处替换为heroku app name> #应该可以从下面那个链接的 deploy using heroku git中找到

git push heroku master
```

更详细的在[这里](https://dashboard.heroku.com/apps/twitter-bot-jackalsin/deploy/heroku-git)

## 3. 参数设置

```bash
module.exports = {
  twitterKeys: {
    consumer_key: process.env.TWITTER_CONSUMER_KEY,
    consumer_secret: process.env.TWITTER_CONSUMER_SECRET,
    access_token: process.env.TWITTER_ACCESS_TOKEN,
    access_token_secret: process.env.TWITTER_ACCESS_TOKEN_SECRET
  },
  twitterConfig: {
    follower_ids :["realDonaldTrump"], #你要跟踪的tweet的screen_name
    max_twitter_lookback: 15, #每次我们往回找多少个tweets来favorite和retweet
    trigger_time_in_millisecond: 1000 * 60 * 60 # 隔多少毫秒重新trigger一次，不确定就不要改
  }
```

## 4. Debug

`heroku logs --tail`

## 5. 更新

每次更新了，都要

```bash
git add .
git commit -am "make it better"
git push heroku master
```

## 6. Reference

待补充

## 7. License

只求大家一件事：

1. 差评保护

Heghlu'meH QaQ jajvam

Carpe Diem
