const config = require('../config')
const Twit = require('twit')
const bot = new Twit(config.twitterKeys)

const workload = (follower_id) => bot.get("statuses/user_timeline", {
    // this is params
    screen_name: follower_id,
    count: config.twitterConfig.max_twitter_lookback
}, (err, data, response) => {
    if (err) {
        console.error("error in timeline: ", err);
    }
    console.log("Running: ", follower_id);
    for (i = 0; i < data.length; ++i) {
        // check if it's retweeted
        console.log(data[i].id_str, data[i].retweeted);
        console.log(data[i].id_str, data[i].favorited);
        if (should_retweet(data[i])) {
            console.log('Retweeting!');
            bot.post("statuses/retweet/:id", { id: data[i].id_str });
        }

        if (should_favorited(data[i])) {
            bot.post("favorites/create", { id: data[i].id_str });
        }
    }
});

const should_retweet = (t) => {
    // already retweeted 
    return !t.retweeted
}

const should_favorited = (t) => {
    // already favorited
    return !t.favorited;
}

const multiple_workload = () => {
    console.log("垂死病中惊坐起ing " + new Date());
    for (i = 0; i < config.twitterConfig.follower_ids.length; ++i) {
        workload(config.twitterConfig.follower_ids[i]);
    }
}

module.exports = multiple_workload
